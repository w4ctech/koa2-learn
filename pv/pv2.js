function pv2(ctx) {
    global.console.log('pv2.js')
}


module.exports = function () {
    return async function (ctx,next) {
        console.log("pv2 start")
        pv2(ctx)
        await next()
        console.log("pv2 end")
    }
}
