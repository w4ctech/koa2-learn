function pv3(ctx) {
    global.console.log('pv3.js')
}


module.exports = function () {
    return async function (ctx,next) {
        console.log("pv3 start")
        pv3(ctx)
        await next()
        console.log("pv3 end")
    }
}
