function pv1(ctx) {
    global.console.log('pv1.js')
}


module.exports = function () {
    return async function (ctx,next) {
        console.log("pv1 start")
        pv1(ctx)
        await next()
        console.log("pv1 end")
    }
}
