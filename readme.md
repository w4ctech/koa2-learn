## 一. 安装
1. 安装`koa2` ``cnpm i -g koa-generator``
2. 初始化项目 ``koa2 project``-e 代表使用模板引擎ejs ``koa2 -e project``
3. `koa2`中没有``windows``对象，用``global``
4. ``npm run dev``能够实时同步修改代码的变化
5. 执行``package.json``中的``scripts``命令时，中的``start``和``test``命令可以直接执行为``npm start``和``npm test``，其他三个必须用``npm run xxx``

## 二. asyns和await语法
1. ``await``什么时候用?
     1. 外层没有声明``async``,不是一个异步函数，函数体里不允许用``await``。换句话说,声明了``async``,函数体里没有``await``是可以的。但是如果函数体里有``await``,外层函数必须声明``async``
2. ``await实例``
```javascript
 router.get('/testAsync',async (ctx)=>{
      global.console.log('start',new Date().getTime(),ctx)
      const a = await new Promise((resolve ,reject)=>{
        setTimeout(function () {
          global.console.log('async a',new Date().getTime(),ctx)
          resolve('a')
        },1000)
      });
      ctx.body = {a}
    })
 ```
 当访问页面``http://localhost:3000/testAsync`` 时 输出的``global.console.log('start',new Date().getTime(),ctx)``值为 ``start 1565235872940``，``   global.console.log('async a',new Date().getTime(),ctx)``值为``async a 1565235873942 ``
渲染整个页面时长``GET /testAsync - 1006ms``
```javascript
router.get('/noawait',async (ctx)=>{
  global.console.log('start',new Date().getTime(),ctx)
  const a = new Promise((resolve ,reject)=>{
    setTimeout(function () {
      global.console.log('async a',new Date().getTime(),ctx)
      resolve('a')
    },1000)
  });
  ctx.body = {a}
})
```
当访问页面``http://localhost:3000/testAsync`` 时页面显示a的值为{}，a的结果就是`` resolve('a')``返回的结果
通过``async````await``代码能够顺序执行，``await``不用写回调，不是用的``Promise``的方式
```javascript
router.get('/testAsync',async (ctx)=>{
  global.console.log('start',new Date().getTime(),ctx)
  const a = await new Promise((resolve ,reject)=>{
    setTimeout(function () {
      global.console.log('async a',new Date().getTime(),ctx)
      resolve('a')
    },1000)
  });
  const b = await 99 //等同于 const b = await Promise.resolve(99)
  const c = await Promise.resolve("Promise.reslove")
  ctx.body = {a,b,c}
})

```
 ``const b = await 99`` 写法等同于 ``const b = await Promise.resolve(99)``

## 三. koa2中间件
1. koa2中间件先进后出
2. koa2中间件必须先``await next()``,否则没法进行到下一个中间件

## 四. koa2路由和cookie
1. 路由的写法
    1. 
    ```javascript
    const router = require('koa-router')() // 引入koa-router
    
    router.prefix('/users') // 使用router.prefix实例化路由
    
    router.get('/', function (ctx, next) {  // 配置路由
      ctx.body = 'this is a users response!'  // 响应
    })
    
    module.exports = router // 导出路由
    
    ```

    2. 在app.js中使用
    ```javascript
    app.use(users.routes(), users.allowedMethods())
    ```
 2. cookie和session
    1. 设置cookie
    ```javascript
    ctx.cookies.set('pvid', Math.random());
                 //  cookie名,cookie值
    ```
    2. 读cookie
    ```javascript
    cookie: ctx.cookies.get('pvid')
    ```
    
    ```ejs
    <p>cookie is <%= cookie %></p>
    ```   
    2. session
        1. 引入session ```npm i koa-session```
        2. 使用``ctx.session`` 来操作session
            1. 
            ```javascript
            const session = require('koa-session')  // 引入session
            app.keys = ['secret']   // session加密字段
            app.use(session({}, app))
            app.use(async (ctx, next) => {
              if (ctx.url === '/index') {
                ctx.session.sessionInfo = true
                ctx.body = {
                  msg: 'session设置成功'
                }
              }
              await next()
            })
            app.use(async (ctx, next) => {
              if (ctx.url === '/users') {
                ctx.session = null
                ctx.body = {
                  msg: 'session为null'
                }
              }
              await next()
            })
            app.use(async ctx => {
              console.log(ctx.session)
              if (ctx.url === '/index') {
                if (ctx.session.sessionInfo === true) {
                  ctx.body = {
                    msg: '成功获取到session'
                  }
                } else {
                  ctx.body = {
                    msg: '获取session失败'
                  }
                }
              }
            })
            app.listen(8000)           
            ```
## 五. mongodb
1. mongodb的概念和安装

   1. mongodb是非关系型数据库
      SQL术语/概念 | MongoDB术语/概念 |  解释/说明  
      -|-|-
      database| database | 数据库
      table| collection | 数据库表/集合
      row| document | 数据记录行/文档
      column| field | 数据字段/域
      index| index | 索引 
      table joins	|  |  表连接,MongoDB不支持
      primary key	| primary key	 | 主键,MongoDB自动将_id字段设置为主键


