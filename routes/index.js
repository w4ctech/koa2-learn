const router = require('koa-router')()

router.get('/', async (ctx, next) => {
  let date = new Date();
  console.log(date)
    ctx.cookies.set('pvid', Math.random());
    await ctx.render('index', {
    title: 'Hello Koa 2!',
    cookie: ctx.cookies.get('pvid')
  })
})

router.get('/string', async (ctx, next) => {
  ctx.body = 'koa2 string'
})

router.get('/json', async (ctx, next) => {
  ctx.body = {
    title: 'koa2 json',
    cookie: ctx.cookies.get('pvid')
  }
})

router.get('/testAsync',async (ctx)=>{
  global.console.log('start',new Date().getTime(),ctx)
  const a = await new Promise((resolve ,reject)=>{
    setTimeout(function () {
      global.console.log('async a',new Date().getTime(),ctx)
      resolve('a')
    },1000)
  });
  const b = await 99 //等同于 const b = await Promise.reslove(99)
  const c = await Promise.resolve("Promise.reslove")
  ctx.body = {a,b,c}
})

router.get('/noawait',async (ctx)=>{
  global.console.log('start',new Date().getTime(),ctx)
  const a = new Promise((resolve ,reject)=>{
    setTimeout(function () {
      global.console.log('async a',new Date().getTime(),ctx)
      resolve('a')
    },1000)
  });
  ctx.body = {a}
})
module.exports = router
