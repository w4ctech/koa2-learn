const Koa = require('koa')
const app = new Koa()
const views = require('koa-views')
const json = require('koa-json')
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')
const logger = require('koa-logger')
const pv =require('./pv/pv')
const pv1 =require('./pv/pv1')
const pv2 =require('./pv/pv2')
const pv3 =require('./pv/pv3')
const session = require('koa-session')
app.keys = ['secret']   // session加密字段
app.use(session({}, app))
const index = require('./routes/index')
const users = require('./routes/users')


// error handler
onerror(app)

// middlewares
app.use(bodyparser({
  enableTypes:['json', 'form', 'text']
}))
app.use(json())
app.use(logger())
app.use(require('koa-static')(__dirname + '/public'))
app.use(pv())
app.use(pv1())
app.use(pv2())
app.use(pv3())

app.use(views(__dirname + '/views', {
  extension: 'ejs'
}))

// logger
app.use(async (ctx, next) => {
  const start = new Date()
  await next()
  const ms = new Date() - start
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
})

// routes
app.use(index.routes(), index.allowedMethods())
app.use(users.routes(), users.allowedMethods())

// error-handling
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
});

app.use(async (ctx, next) => {
  if (ctx.url === '/index') {
    ctx.session.sessionInfo = true
    ctx.body = {
      msg: 'session设置成功'
    }
  }
  await next()
})
app.use(async (ctx, next) => {
  if (ctx.url === '/users') {
    ctx.session.sessionInfo = null
    ctx.body = {
      msg: 'session为null'
    }
  }
  await next()
})
app.use(async ctx => {
  console.log(ctx.session)
  if (ctx.url === '/index') {
    if (ctx.session.sessionInfo === true) {
      ctx.body = {
        msg: '成功获取到session'
      }
    } else {
      ctx.body = {
        msg: '获取session失败'
      }
    }
  }
})
app.listen(8000)
module.exports = app
